/*
 * -------------------------------------------------
 *  Nextflow test config file for processes options
 * -------------------------------------------------
 * Defines general paths for input files and
 * parameters for ORSON processes
 */

params {

    // Analyzed project name
    projectName = "orson_custom_test"
    // Output directory to publish workflow results
    outdir = "${baseDir}/results/${projectName}"

    // Input FASTA file
    fasta = "${baseDir}/test_dataset/query.fa"
    // Type of data - for nucleic acid sequences: "n"; for protein sequences: "p"
    query_type = "p"
    // Number of sequences in each fasta chunk
    chunk_size = 20

    /*
    Steps to activate or deactivate
    */
    get_singularity_enable = true
    downloadDB_enable = false
    busco_enable = true
    iprscan_enable = true
    beedeem_annot_enable = true
    eggnogmapper_enable = true
    
    /*
    For transcriptome annotation and if you have enabled the BUSCO process (params.busco_enable), set the BUSCO lineage to use, and the path to the BUSCO databases. For multiple lineages analysis, set a list of desired lineages separated by commas. 
    */
    lineage = "primates_odb10"
    busco_db = "${baseDir}/busco_db"

    /*
    Tool used for the sequence comparison. Can be BLAST or diamond.
    */
    hit_tool = "diamond"

    /*
    Installing annotated sequence banks
    */
    db_dir = "${baseDir}/databases"
    bank_list = "GeneOntology_terms,InterPro_terms,Enzyme,NCBI_Taxonomy,Uniprot_SwissProt"

    /*
    If hit_tool = "BLAST" or "diamond" and you want to use another annotated sequence banks, modify the following parameters:
    */
    restricted_search = false
    restricted_tax_id = "none"
    blast_db = "${db_dir}/p/Uniprot_SwissProt/current/Uniprot_SwissProt/Uniprot_SwissProt"
    
    /* 
    If hit_tool = "diamond" and you want to be more sensitive, modify the following parameter.
    Allowed values: 'fast','mid-sensitive','sensitive','more-sensitive','very-sensitive','ultra-sensitive'
    */
    sensitivity = "fast"

    /*
    Set the number of hits per query for BLAST and Diamond.
    */
    max_target_seq = 5

    /*
    Annotation of hits using BeeDeeM. Can be "bco" or "full".
    See https://pgdurand.gitbook.io/beedeem/utils/cmdline-annotate for more details.
    */
    annot_type = "bco"
}
