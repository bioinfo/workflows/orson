# A simple BeeDeeM descriptor to retrive BUSCO test data set.

db.name=primates_odb10
db.desc=primates_odb10 subset from BUSCO V5 databank 
db.type=d
db.ldir=${mirrordir}|d|primates_odb10
db.provider=SeBiMER

db.files.include=busco-v5-primates_odb10.tar.gz
db.files.exclude=

tasks.unit.post=gunzip,untar

tasks.global.post=deltar,delgz

ftp.uname=anonymous
ftp.pswd=bioinfo@ifremer.fr
ftp.server=ftp.ifremer.fr
ftp.port=21
ftp.rdir=/ifremer/dataref/bioinfo/sebimer/tools/ORSON/data
ftp.rdir.exclude=

history=0

