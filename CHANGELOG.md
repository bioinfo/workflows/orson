# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## In progress

### Added
- Issue 43: 
    - enable to control max_target_seq from conf file
    - can be used to control how many hits reported per query by similarity search tools (BLAST and Diamond)
- Issue 44: 
    - enable better control over resources for beedeem_annotator task; see conf/resources.config
    - param task.memory now used to pass memory setting to Java -Xmx argument

### Changed

### Fixed


## Before [1.2.0]

Versions prior to 1.2.0: no chnage log at that time