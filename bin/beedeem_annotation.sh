#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: Run hit annotation using BeeDeeM                       ##
##                                                                           ##
###############################################################################

# var settings 
args=("$@")
DB_DIR=${args[0]}
WK_DIR=${args[1]}
XML_FILES=${args[2]}
ZML_OUT=${args[3]}
TYPE=${args[4]}
# Memory format of Nextflow (https://www.nextflow.io/docs/latest/reference/process.html#memory)
# is <int><space><unit>, e.g.: 8 GB 
MEMORY=$(echo ${args[5]} | cut -d' ' -f1)
LOGCMD=${args[6]}
CHUNK_NAME=$(basename ${XML_FILES%.*})
CHUNK_ZML_OUT_NAME=${CHUNK_NAME}_${ZML_OUT}

# Run BeeDeeM
export KL_mirror__path=$DB_DIR
export KL_WORKING_DIR=$WK_DIR
# About memory: we consider process memory (see conf/resources.config) is set using
# GB units. 
export KL_JRE_ARGS="-Xms2G -Xmx${MEMORY}G -Djava.io.tmpdir=$KL_WORKING_DIR -DKL_LOG_TYPE=console"
CMD="bdm annotate -i $XML_FILES -o $CHUNK_ZML_OUT_NAME -type $TYPE -writer zml"

echo "KL_mirror__path: $KL_mirror__path" > $LOGCMD
echo "KL_WORKING_DIR : $KL_WORKING_DIR" >> $LOGCMD
echo "KL_JRE_ARGS    : $KL_JRE_ARGS" >> $LOGCMD
echo $CMD >> $LOGCMD

eval $CMD
