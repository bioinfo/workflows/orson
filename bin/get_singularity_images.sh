#!/usr/bin/env bash
###############################################################################
##                                                                           ##
## Purpose of script: get singularity images of orson dependencies           ##
##                                                                           ##
###############################################################################

args=("$@")
BASEDIR=${args[0]}
SING_IMAGES_OK=${args[1]}

# Local folder to store ORSON dependency Singularity images
container_dir="$BASEDIR/containers"

# ORSON dependency URL
ORSON_FTP="ftp://ftp.ifremer.fr/ifremer/dataref/bioinfo/sebimer/tools/ORSON"

# Will be used to figure out whether or not images retrieval is ok
TASK_OK=0

# Images to download
SIF_IMGS=( \
  "beedeem-5.0.2.sif" \
  "blast-2.13.0.sif" \
  "busco-5.4.3.sif" \
  "diamond-2.0.15.sif" \
  "eggNOG-mapper-2.1.9.sif" \
  "iprscan-5.59-91.0.sif" 
)

# A little check (possible error during code dev)
if [ ! $? -eq 0 ]; then
  exit 1
fi

# Start processing
for SIF_IMG in "${SIF_IMGS[@]}"; do
  echo "> Processing: $SIF_IMG"
  # Default: we have to load image
  DO_LOAD=1
  SIF_URL=$ORSON_FTP/$SIF_IMG
  LCL_SIF_IMG=$container_dir/$SIF_IMG
  # Get remote file size to download
  SIF_STAT=${LCL_SIF_IMG}.txt
  wget --spider "$SIF_URL" > $SIF_STAT 2>&1
  SIF_REF_SIZE=$(cat $SIF_STAT | grep "==> SIZE" | rev | cut -d' ' -f1 | rev)
  rm $SIF_STAT
  echo "  ${SIF_IMG} on remote server: $SIF_REF_SIZE bytes"
  # If image exists, check file size
  if [ -f "$LCL_SIF_IMG" ]; then
    echo "  $SIF_IMG, already there, check file size..."
    SIR_CUR_SIZE=$(stat --printf="%s" $LCL_SIF_IMG)
    echo "  ${SIF_IMG} in local folder: $SIR_CUR_SIZE bytes"
    if [[ "$SIF_REF_SIZE" -eq "$SIR_CUR_SIZE" ]]; then
      echo "  skipping for download (file size ok)"
      #The only case for which no download is required
      DO_LOAD=0
    else
      echo "  file size not ok, force download"
    fi
  fi 
  if [ $DO_LOAD -eq 1 ]; then 
    echo "  downloading $SIF_IMG"
    CMD="wget -c \"$ORSON_FTP/$SIF_IMG\" -O $LCL_SIF_IMG"
    echo $CMD
    eval $CMD
    if [ ! $? -eq 0 ]; then
      # Remove possible empty/incomplete file
      rm -f $LCL_SIF_IMG
      TASK_OK=1
    fi 
  fi
done

# Ensure $SING_IMAGES_OK is created only when everything if ok
rm -f $SING_IMAGES_OK
if [ $TASK_OK -eq 0 ]; then
  echo "all Singularity images OK"
  touch $SING_IMAGES_OK
fi

# Leave script with error code if any
exit $TASK_OK

