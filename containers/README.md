# ORSON: Singularity recipes

In this directory, you can find all the singularity recipes allowing you to create manually all the dependencies necessary for ORSON. As a reminder, ORSON automatically downloads the dependencies from the SeBiMER FTP server 

The recipes is split into the following files:

1. [BLAST singularity recipe](Singularity.blast-2.13.0)
2. [Diamond singularity recipe](Singularity.diamond-2.0.15)
3. [eggNOG-mapper singularity recipe](Singularity.eggNOG-mapper-2.1.9)
4. [InterProScan singularity recipe](Singularity.interproscan-5.59-91.0)

The BeeDeeM singularity recipe is available in the [BeeDeeM GitHub project](https://github.com/pgdurand/BeeDeeM/blob/master/singularity/Singularity)
